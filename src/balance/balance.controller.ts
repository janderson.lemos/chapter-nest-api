import { Controller, Get, Param, UseInterceptors } from '@nestjs/common';
import { BalanceDTO } from './dtos/balance.dto';
import { ApiTags, ApiResponse } from '@nestjs/swagger';
import { HttpLogger } from '../interceptors/http-logger.interceptor';
import { WebfeederService } from '../webfeeder/webfeeder.service';

@ApiTags('balance')
@Controller('balance')
export class BalanceController {

    constructor(private webfeederService: WebfeederService) { }

    @HttpLogger()
    @Get('/:customerId')
    @ApiResponse({
        status: 200, description: 'Dados de saldo do cliente',
        type: BalanceDTO,
    })
    async byCustomerId(@Param('customerId') customerId: string): Promise<BalanceDTO> {
        const balance = await this.webfeederService.getBalanceByCustomerId(customerId)
        return new BalanceDTO({
            balance,
            transtionValue: '0',
            positionValue: '0'
        })
    }

}
