import { ApiProperty } from "@nestjs/swagger"


export class BalanceDTO {
    @ApiProperty()
    public balance: string
    @ApiProperty()
    public transtionValue: string
    @ApiProperty()
    public positionValue: string

    constructor(props: Readonly<BalanceDTO>){
        Object.assign(this, props)
    } 
}