import { Module } from '@nestjs/common';
import { BalanceController } from './balance.controller';
import { WebfeederModule } from 'src/webfeeder/webfeeder.module';

@Module({
  imports: [WebfeederModule],
  controllers: [BalanceController],
})
export class BalanceModule { }
