import { Injectable, NestMiddleware, Logger } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  constructor(private logger: Logger) { }
  use(req: Request, res: Response, next: () => void): void {
    this.logger.log(req.baseUrl, 'LoggerMiddleware');
    next();
  }
}
