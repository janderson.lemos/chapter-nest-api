import { Test, TestingModule } from '@nestjs/testing';
import { WebfeederService } from './webfeeder.service';

describe('WebfeederService', () => {
  let service: WebfeederService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WebfeederService],
    }).compile();

    service = module.get<WebfeederService>(WebfeederService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
