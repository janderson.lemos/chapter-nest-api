import { Injectable } from '@nestjs/common';
@Injectable()
export class WebfeederService {
    async getBalanceByCustomerId(id: string): Promise<string> {
        if (id === '1') {
            return '100'
        }
        return '1000000'
    }
}
