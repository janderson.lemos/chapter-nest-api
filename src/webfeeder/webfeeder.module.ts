import { Module } from '@nestjs/common';
import { WebfeederService } from './webfeeder.service';

@Module({
  providers: [WebfeederService],
  exports: [WebfeederService]
})
export class WebfeederModule { }
