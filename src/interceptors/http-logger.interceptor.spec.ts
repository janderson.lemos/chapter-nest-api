import { HttpLoggerInterceptor } from './http-logger.interceptor';
import { Reflector } from '@nestjs/core';
import { Logger } from '@nestjs/common';

describe('HttpLoggerInterceptor', () => {
  it('should be defined', () => {
    expect(new HttpLoggerInterceptor(new Reflector(), new Logger())).toBeDefined();
  });
});
