import { CallHandler, ExecutionContext, Injectable, NestInterceptor, SetMetadata, Logger } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';

export interface IHttpLoggerDecoratorValues {
  className: string,
  method: string
}
// eslint-disable-next-line @typescript-eslint/ban-types
export const HttpLogger = () => function (target: Object, key: string, descriptor: TypedPropertyDescriptor<Function>): void {
  SetMetadata('http-logger', {
    className: target.constructor.name,
    method: key
  } as IHttpLoggerDecoratorValues)(target, key, descriptor)
}

@Injectable()
export class HttpLoggerInterceptor implements NestInterceptor {
  constructor(
    private reflector: Reflector,
    private logger: Logger
  ) { }

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request: Request = context.switchToHttp().getRequest()
    return next.handle().pipe(tap(responseData => {
      const response = context.switchToHttp().getResponse()
      const decoratorValues = this.reflector.get<IHttpLoggerDecoratorValues>('http-logger', context.getHandler())
      if (decoratorValues) {
        this.logger.log(JSON.stringify({
          ...decoratorValues,
          url: request.url,
          query: request.query,
          params: request.params,
          body: request.body,
          response: {
            statusCode: response.statusCode,
            data: responseData
          }
        }), 'HttpLoggerInterceptor')
      }
    }))
  }

}
